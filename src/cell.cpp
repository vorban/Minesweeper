#include "cell.h"

/** ------------ */
/** CONSTRUCTORS */
/** ------------ */

Cell::Cell()
{
	this->trapped = false;
	this->revealed = false;
	this->neighbours = 0;
}

Cell::Cell(bool trapped)
{
	this->trapped = trapped;
	this->revealed = false;
	this->neighbours = 0;
}

/** --------- */
/** FUNCTIONS */
/** --------- */

char Cell::getDisplay()
{
	if (!revealed)
		return 219;
	if (trapped)
		return 197;
	return (neighbours == 0) ? 176 : neighbours + 48;
}

