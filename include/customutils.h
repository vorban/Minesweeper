#ifndef CUSTOMUTILS_H_INCLUDED
#define CUSTOMUTILS_H_INCLUDED

#include <vector>
#include <string>
#include <iostream>

namespace customutils
{
	std::vector<int> getNInput(int n);
}

#endif // CUSTOMUTILS_H_INCLUDED
