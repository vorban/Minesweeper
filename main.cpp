#include <iostream>
#include <string>
#include <vector>

#include "minesweeper.h"

using namespace std;

int main()
{
	cout << "Hello and welcome to Minesweeper !" << endl;
	Minesweeper ms = Minesweeper();
	ms.setup();
	ms.run();

	return 0;
}
